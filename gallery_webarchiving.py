from webarchiving import Spider

spider = Spider("gallery", preserve_query=False, skip_existing_files=True)

### HTML

spider.add_pattern(
    r"^https://gallery3?\.constantvzw\.org/?$",
    "index.html")
spider.add_pattern(r"^https://gallery3?\.constantvzw\.org/index\.php/downloadalbum/", "")
spider.add_pattern(
    r"^https://gallery3?\.constantvzw\.org/index.php/(\?show=\d+)?$",
    "index.php/index.html")
spider.add_pattern(
    r"^https://gallery3?\.constantvzw\.org/index.php/\?page=(\d+)$",
    r"index.php/index_page\g<1>.html")

# why is this pattern necessary beyond below ?
# spider.add_pattern(
#     r"^https://gallery3?\.constantvzw\.org/var/thumbs/([^\?]+?)/\.album\.jpg\?m=(\d+)$",
#     r"g/\g<1>/album_thumb.jpg")

# gallery
spider.add_pattern(
    r"^https://gallery3?\.constantvzw\.org/index.php/([^\?]+?)(\?show=\d+)?$",
    r"index.php/\g<1>/index.html")
# gallery pages
spider.add_pattern(
    r"^https://gallery3?\.constantvzw\.org/index.php/([^\?]+?)\?page=(\d+)$",
    r"index.php/\g<1>/index_page\g<2>.html")
# tag pages
spider.add_pattern(
    r"^https://gallery3?\.constantvzw\.org/index.php/tag/(\d+)/([^\/\?]+)$",
    r"index.php/tag/\g<2>/index.html")

#############
# IMAGES -- keep them where they are ?!
spider.add_pattern(
    r"^https://gallery3?\.constantvzw\.org/var/(thumbs|resizes|albums)/([^\?]+?)\?m=(\d+)$",
    r"var/\g<1>/\g<2>")

# https://gallery.constantvzw.org/index.php/tag/226/Libre+Objet


# spider.add_pattern(r"^https://gallery.constantvzw.org/index.php\?/categories$", "index.html")
# spider.add_pattern(r"^https://piwigo.constantvzw.org/index.php\?/categories/startcat-(\d+)$", "index-\g<1>.html")
# spider.add_pattern(r"^https://piwigo.constantvzw.org/index.php\?/category/(\d+)$", "gallery-\g<1>.html")

#leave images in place




test_urls = """
https://gallery.constantvzw.org/index.php/?show=36494
https://gallery.constantvzw.org/index.php/Constant_V-next-Iterations?show=36266
https://gallery.constantvzw.org/var/thumbs/Constant-in-het-bos/.album.jpg?m=1602191385
https://gallery.constantvzw.org/var/thumbs/Constant_V-Constant_F/PWFU2433.JPG?m=1620629779
https://gallery.constantvzw.org/index.php/Alchorisma_Summercamp?page=2
https://gallery.constantvzw.org/var/thumbs/APT/browsing/.album.jpg?m=1454584296
https://gallery.constantvzw.org/var/thumbs/APT/browsing/PWFU2433.JPG?m=1620629779
https://gallery.constantvzw.org/index.php/APT/browsing
https://gallery.constantvzw.org/index.php/APT/browsing?page=2
https://gallery.constantvzw.org/index.php/downloadalbum/zip/album/1
https://gallery.constantvzw.org/var/albums/ddd1/20210717_155234_JC.jpg?m=1627734630
"""

for url in test_urls.strip().splitlines():
    if url:
         print (f"{url}:\t{spider.url_to_local_path(url)}")

# spider.spider("https://gallery3.constantvzw.org/", sleep=0.010)
