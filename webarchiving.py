import requests
from urllib.parse import urljoin, urlparse, quote as urlquote, unquote as urlunquote
import html5lib
from xml.etree import ElementTree as ET
import os
import re
import sys
from hashlib import md5
import time

import urllib3
# https://stackoverflow.com/questions/27981545/suppress-insecurerequestwarning-unverified-https-request-is-being-made-in-pytho
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

"""
When expecting an image... to enforce mime, or at least not then follow HTML

EXAMPLE:

http://www.getmiro.com/img/i/shot1.png

http://metavid.ucsc.edu/images/interface.jpg


DONE:
Extra checks before recursively following text/html response
Ensure that we are expecting a local .html


"""


EXT = {
    "text/html": "html",
    "text/css": "css",
    "image/jpeg": "jpg",
    "image/png": "png",
    "image/gif": "gif",
    "image/svg+xml": "svg",
    "application/javascript": "js",
    "text/javascript": "js",
    "video/webm": "webm",
    "font/ttf": "ttf",
    "font/woff": "woff",
    "application/vnd.ms-fontobject": "eot",
    "application/font-woff": "woff",
    "application/rss+xml": "rss"
}

def memoize(f):
    memo = {}
    def helper(x):
        if x not in memo:            
            memo[x] = f(x)
        return memo[x]
    return helper

@memoize
def ext_for (url):
    # try / allow simple extension test to override
    parts = urlparse(url)
    if parts.path:
        ext = os.path.splitext(parts.path)[1].lstrip(".").lower()
        if ext in ('html', 'js', 'css', 'gif', 'jpg', 'png', 'jpeg', 'mp3', 'ogg', 'ogv', 'webm', 'mp4'):
            return ext
    try:
        r = requests.head(url, verify=False)
        ct = r.headers['content-type'].split(";")[0]
        if ct not in EXT:
            print (f"Warning, unknown extension for content-type {ct}, using bin", file=sys.stderr)
        return EXT.get(ct, "bin")
    except Exception as e:
        print (f"Exception {url}: {e}", file=sys.stderr)
        return "bin"
    
def innerHTML (elt):
    if elt.text != None:
        ret = elt.text
    else:
        ret = u""
    return ret + u"".join([ET.tostring(x, method="html", encoding="unicode") for x in elt])

def setInnerHTML (elt, text):
    contents = list(elt)
    for c in contents:
        elt.remove(c)
    elt.text = text

def split_fragment(href):
    try:
        ri = href.rindex("#")
        return href[:ri], href[ri:]
    except ValueError:
        return href, ''

def split_query(href):
    try:
        ri = href.rindex("?")
        return href[:ri], href[ri:]
    except ValueError:
        return href, ''

class NoException (Exception):
    pass

def srcset_sub (val, repl):
    print (f"srset_sub {val}")
    ret = ""
    parts = re.split("(, *)", val)
    i = 0
    while i<len(parts):
        if i%2 == 0:
            sret = ""
            sparts = re.split("( +)", parts[i])
            j = 0
            while j<len(sparts):
                if j == 0:
                    print (f"srcset sub {i},{j} {sparts[j]}")
                    sret += repl(sparts[j])
                else:
                    sret += sparts[j]
                j += 1
            ret += sret
        else:
            ret += parts[i]
        i += 1
    return ret

class Spider:
    def __init__(self, output_path=".", \
                 skip_existing_files=False, \
                 pattern_flags=0, \
                 verbose=False, \
                 preserve_query=True, \
                 additional_attributes=None,
                 encoding=None):
        self.verbose = verbose
        self.pattern_flags = pattern_flags
        self.patterns = []
        self.exceptions = []
        self.output_path = output_path
        self.skip_existing_files = skip_existing_files
        self.preserve_query = preserve_query
        self.additional_attributes = []
        self.encoding = encoding
        if additional_attributes:
            if type(additional_attributes) == str:
                self.additional_attributes.append(additional_attributes)
            else:
                self.additional_attributes.extend(additional_attributes)
        self.rewrites = []

    def add_pattern (self, search, replace, post_process=None):
        """ nb:the replace should always form a local path, with no query
            as re.sub is used, the search should probably capture the entire string ^$
            otherwise unmatched trailing stuff (for instance) can suddenly appear at the end
            (would this be a nicer way to allow queries to be preserved?? ... but then would need to change the code to reparse query in the local path)
        """
        if type(search) == str:
            search = re.compile(search, flags=self.pattern_flags)
        self.patterns.append((search, replace, post_process))

    def add_exception (self, pattern):
        """
        Content matching this pattern will *not* be downloaded/localized
        even if included/embedded as an image / video
        """
        if type(pattern) == str:
            pattern = re.compile(pattern, flags=self.pattern_flags)
        self.exceptions.append(pattern)

    def has_exception(self, url) -> bool:
        for psearch in self.exceptions:
            m = psearch.search(url)
            if m:
                return True
        return False
        
    def sub_pattern (self, url):
        for psearch, preplace, post_process in self.patterns:
            m = psearch.search(url)
            if m:
                # MM: adding 10 feb 2023 in context of gallery
                # allow blanks to be exceptions (need ability to prevent some links from being followed)
                # in this case download as zip links that otherwise match
                if preplace == "":
                    return None
                ret = psearch.sub(preplace, url)
                if post_process:
                    ret = post_process(ret)
                return ret

    def url_to_local_path (self, url):
        """ conditional: return str or None """
        ret = self.sub_pattern(url)
        if ret:
            ret = urlunquote(ret)
        return ret

    def generic_url_to_path (self, url):
        """ todo: if there is a filename use it -- as well ?! """
        md5hash = md5(url.encode()).hexdigest()
        parts = urlparse(url)
        ext = ext_for(url)
        filename = os.path.basename(parts.path)
        if filename:
            filename, _ = os.path.splitext(filename)
            return f"ext/{md5hash}/{filename}.{ext}"
        else:
            return f"ext/{md5hash}.{ext}"

    def url_to_path (self, url) -> str:
        """ unconditional: always return str """
        l = self.url_to_local_path(url)
        if l:
            return l
        else:
            return self.generic_url_to_path(url)

    def localize (self, url) -> str:
        """
        schedules (unless already done) a URL for download and (unconditionally) maps to a local path
        returns: local path
        """
        if url not in self.done and url not in self.todo:
            self.todo.append(url)
        ret = self.url_to_path(url)
        # print (f"localize {url} => {ret}")
        return ret

    def has_local_path(self, url) -> bool:
        return self.url_to_local_path(url) is not None

    def relpath (self, to_file, from_file):
        return os.path.relpath(to_file, os.path.dirname(from_file))

    def add_resource_url (self, t, url):
        if t.tag == "html":
            html_tag = t
        else:
            html_tag = t.find("./html")
        if html_tag is not None:
            assert 'resource' not in html_tag
            html_tag.attrib['resource'] = url

    def add_meta (self, t, url):
        head = t.find(".//head")
        assert head is not None
        metaroot = ET.SubElement(head, "link", rel="original", href=url)

    def should_localize(self, additional_attributes, elt, attribname, href):
        """
            * src: always localized unless there's an exception (keep live)
            * link rel=stylesheet: always localized
            * regular links & additional_attrs, if has_local_path
        """
        return (elt.tag in ["img", "video", "audio", "script"] and attribname == "src" and not self.has_exception(href)) or \
                (elt.tag == "link" and elt.attrib.get("rel") == "stylesheet") or \
                ((elt.tag == "a" or attribname in additional_attributes) and self.has_local_path(href))
                                    
    def download(self, url):
        path = self.url_to_path(url)
        usepath = os.path.join(self.output_path, path)
        if self.skip_existing_files and os.path.exists(usepath):
            if self.verbose:
                print ("File already exists, skipping...")
            return # why do I need to add this back ?! (2021-03-06)
        #if self.verbose:
        additional_attributes = []
        if self.additional_attributes:
            additional_attributes.extend(self.additional_attributes)
        all_attributes = ["src", "href"] + additional_attributes
        self.rewrites.append((url, usepath))
        print (f"{url} => {usepath}")
        try:
            if os.path.dirname(usepath):
                os.makedirs(os.path.dirname(usepath), exist_ok=True)
            r = requests.get(url, verify=False)
            # 2022 09 30: First implementation would spider ALL text/html responses...
            # in practice: Old media links can often lead link heavy HTML pages
            # Only recursively follow these links if we're in the context of a local HTML
            # if r.headers["content-type"].startswith("text/html"):
            if r.headers["content-type"].startswith("text/html") and \
                self.has_local_path(url) and \
                os.path.splitext(path)[1].lower() == ".html":
                
                if self.encoding:
                    r.encoding = self.encoding
                t = html5lib.parse(r.text, namespaceHTMLElements=False)
                # self.add_resource_url(t, url)
                self.add_meta(t, url)
                
                for attribname in all_attributes:
                    for elt in t.findall(f".//*[@{attribname}]"):
                        href = urljoin(url, elt.attrib.get(attribname))
                        href, fragment = split_fragment(href)
                        if self.preserve_query:
                            href_noquery, query = split_query(href)
                        else:
                            query = ''
                        # print (elt.tag, href, url_to_path(href))
                        # TODO: PROCESS img.srcset!
                        if (elt.tag == 'img' and 'srcset' in elt.attrib):
                            def repl (oref):
                                ref = urljoin(url, oref)
                                if not self.has_exception(ref):
                                    local_ref = self.localize(ref)
                                    ## TODO? centralize the treatment of urls to for instance
                                    ## be consisten about splitting of fragment/query (not currently doing that here, not good?)
                                    return urlquote(self.relpath(local_ref, path))
                                return oref
                            elt.attrib['srcset'] = srcset_sub(elt.attrib['srcset'], repl)
                        if self.should_localize(additional_attributes, elt, attribname, href):
                            if self.verbose:
                                print ("SHOULD LOCALIZE: {href}")
                            local_link = self.localize(href)
                            # need path == current document path
                            elt.attrib[attribname] = urlquote(self.relpath(local_link, path)) + query + fragment
                # TODO: Scan inline CSS...
                for style in t.findall(".//style"):
                    # print ("Processing inline style")
                    # print (style.text)
                    src = self.process_css_src(innerHTML(style), url, path)
                    setInnerHTML(style, src)
                    
                with open(usepath, "w") as fout:
                    print(ET.tostring(t, method="html", encoding="unicode"), file=fout)
            elif r.headers["content-type"].startswith("text/css") or usepath.endswith(".css"):
                # print ("CSS SUBSTITUTION")
                if self.encoding:
                    r.encoding = self.encoding
                # PROCESS CSS_TEXT
                src = self.process_css_src(r.text, url, path)
                with open(usepath, "w") as fout:
                    print(src, file=fout)
            else:
                # print ("Downloading binary...")
                with open(usepath, 'wb') as fd:
                    for chunk in r.iter_content(chunk_size=1024):
                        fd.write(chunk)
        except Exception as e:
            print (f"Exception {url}: {e}", file=sys.stderr)
 
    def process_css_src(self, src, url, path):
        """ url: of processing context, path: of processing context, for url relativation """
        def css_sub(m):
            href = urljoin(url, m.group(2))
            # print (f"CSS SHOULD LOCALIZE {href}", self.should_localize(href))
            # 2022-09-16: localize ALL NON-DATA url references in stylesheets
            # (don't have explicit rules for resources and the URLs are weird)
            # would be nice to be able to write a matcher and use a md5 filter explicitly as a rule
            if not href.startswith("data"): # True or self.should_localize(href):
                href, fragment = split_fragment(href) # TESTING
                local_link = self.localize(href)
                return "url("+m.group(1)+urlquote(self.relpath(local_link, path))+m.group(3)+fragment+")"
            return m.group(0)                            
        return re.sub(r"""url\((['" ]*)(.+?)(['" ]*)\)""", css_sub, src)


    def spider (self, url, sleep=None):
        self.done = set()
        self.todo = [url]
        count = 0
        while self.todo:
            url = self.todo[0]
            self.todo = self.todo[1:]
            self.done.add(url)
            self.download(url)
            count +=1
            if sleep:
                time.sleep(sleep)
            
