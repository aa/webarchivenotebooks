from webarchiving import Spider

spider = Spider("osvideo", skip_existing_files=True)



# MEDIAWIKI
## file pages
spider.add_pattern(r"http://osvideo.constantvzw.org/wiki/index.php/(File:[^?]+)", "wiki/\g<1>.html")
## images
spider.add_pattern(r"http://osvideo.constantvzw.org/wiki/images/([^?]+)", "wiki/images/\g<1>")
## HISTORY PAGES
spider.add_pattern(r"^http://osvideo.constantvzw.org/wiki/index.php\?title=([^&]+)&action=history$", "wiki/\g<1>_history.html")
## regular pages
## spider.add_pattern(r"^http://osvideo.constantvzw.org/wiki/index.php/([\w_\-\:\,\.]+)$", "wiki/\g<1>.html")
spider.add_pattern(r"^http://osvideo.constantvzw.org/wiki/index.php/([^?]+)$", "wiki/\g<1>.html")
# spider.spider("http://osvideo.constantvzw.org/wiki/index.php/Special:AllPages")
# WORDPRESS
spider.add_pattern("^http://osvideo.constantvzw.org/$", "index.html")
spider.add_pattern("^http://osvideo.constantvzw.org/([^?]*?)/$", "\g<1>.html")
spider.add_pattern("^http://osvideo.constantvzw.org/([^?]*)$", "\g<1>")

spider.add_exception("^https?://video.constantvzw.org/(.*)$")
spider.add_exception("^https?://www.getmiro.com/(.*)")

spider.spider("http://osvideo.constantvzw.org/")

# bad_url = "http://osvideo.constantvzw.org/code-rush/"
# spider.spider(bad_url)

# spider = Spider("pesoq", skip_existing_files=True)
# # file pages
# spider.add_pattern(r"http://www.pesoq.constantvzw.org/index.php/(File:[^?]+)", "\g<1>.html")
# # images
# spider.add_pattern(r"http://www.pesoq.constantvzw.org/images/([^?]+)", "images/\g<1>")
# # regular pages
# spider.add_pattern(r"^http://www.pesoq.constantvzw.org/index.php/([^?]+)$", "\g<1>.html")
# spider.spider("http://www.pesoq.constantvzw.org/index.php/Special:AllPages")


# # https://interne.constantvzw.org/index.php/Main_Page
# spider = Spider("interne", skip_existing_files=True)
# # file pages
# spider.add_pattern(r"http://interne.constantvzw.org/index.php/(File:[^?]+)", "\g<1>.html")
# # images
# spider.add_pattern(r"http://interne.constantvzw.org/images/([^?]+)", "images/\g<1>")
# # HISTORY PAGES
# spider.add_pattern(r"^http://interne.constantvzw.org/index.php\?title=([^&]+)&action=history$", "\g<1>_history.html")
# # regular pages
# spider.add_pattern(r"^http://interne.constantvzw.org/index.php/([^?]+)$", "\g<1>.html")
# spider.spider("http://interne.constantvzw.org/index.php/Special:AllPages")


# https://vj13.constantvzw.org/wiki/index.php/Main_Page
# spider = Spider("vj13_mw", skip_existing_files=True)
# # # file pages
# spider.add_pattern(r"http://vj13.constantvzw.org/wiki/index.php/(File:[^?]+)", "\g<1>.html")
# # images
# spider.add_pattern(r"http://vj13.constantvzw.org/wiki/images/([^?]+)", "images/\g<1>")
# # HISTORY PAGES
# spider.add_pattern(r"^http://vj13.constantvzw.org/wiki/index.php\?title=([^&]+)&action=history$", "\g<1>_history.html")
# # regular pages
# # spider.add_pattern(r"^http://vj13.constantvzw.org/wiki/index.php/([\w_\-\:\,\.]+)$", "\g<1>.html")
# spider.add_pattern(r"^http://vj13.constantvzw.org/wiki/index.php/([^?]+)$", "\g<1>.html")
# spider.spider("http://vj13.constantvzw.org/wiki/index.php/Special:AllPages")
